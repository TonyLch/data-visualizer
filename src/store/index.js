import { createStore } from 'vuex'

export default createStore({
  state: () => ({
    // API
    loading: false,
    list: [],
    // Filter by search
    search: '',
    // Selected filter & sort
    selected: {
      gender: 'Both',
      sort: 'Id',
      order: 'Ascending',

      favorite: {
        label: '',
        data: ''
      }
    },
    sorts: {
      // Filter by gender
      gender: [
        {id: 'both', text: 'Both'},
        {id: 'male', text: 'Male'},
        {id: 'female', text: 'Female'},
      ],
      // Sort by one the listed category
      sort: [
        {id: 'id', text: 'Id'},
        {id: 'firstName', text: 'First Name'},
        {id: 'lastName', text: 'Last Name'},
        {id: 'country', text: 'Country'},
        {id: 'pet', text: 'Favorite Pet'},
        {id: 'fruit', text: 'Favorite Fruit'},
        {id: 'color', text: 'Favorite Color'},
        {id: 'movie', text: 'Favorite Movie'},
      ],
      // Sort ascending or descending
      order: [
        {id: 'asc', text: 'Ascending'},
        {id: 'desc', text: 'Descending'},
      ]
    },

    // MODAL
    toggleModal: false,
    id: null
  }),
  getters: {
    // API
    LOADING: state => state.loading,
    LIST: state => state.list,
    SEARCH: state => state.search,
    FILTEREDLIST: state => {
      let sortedList;

      // Filter by gender
      if (state.selected.gender === 'Both')
        sortedList = state.list;
      else if (state.selected.gender === 'Male')
        sortedList = state.list.filter(person => person.gender === 'Male');
      else if (state.selected.gender === 'Female')
        sortedList = state.list.filter(person => person.gender === 'Female');

      // Filter by favorite
      if (state.selected.favorite.label === 'Favorite Pets')
        sortedList = sortedList.filter(person => person.preferences.favorite_pet === state.selected.favorite.data);
      else if (state.selected.favorite.label === 'Favorite Fruits')
        sortedList = sortedList.filter(person => person.preferences.favorite_fruit === state.selected.favorite.data);
      else if (state.selected.favorite.label === 'Favorite Colors')
        sortedList = sortedList.filter(person => person.preferences.favorite_color === state.selected.favorite.data);

      // Sort by category
      if (state.selected.sort === 'Id')
        sortedList.sort((a, b) => a.id > b.id ? 1 : -1);
      else if (state.selected.sort === 'First Name')
        sortedList.sort((a, b) => a.firstname > b.firstname ? 1 : -1);
      else if (state.selected.sort === 'Last Name')
        sortedList.sort((a, b) => a.lastname > b.lastname ? 1 : -1);
      else if (state.selected.sort === 'Country')
        sortedList.sort((a, b) => a.contact.country > b.contact.country ? 1 : -1);
      else if (state.selected.sort === 'Favorite Pet')
        sortedList.sort((a, b) => a.preferences.favorite_pet > b.preferences.favorite_pet ? 1 : -1);
      else if (state.selected.sort === 'Favorite Fruit')
        sortedList.sort((a, b) => a.preferences.favorite_fruit > b.preferences.favorite_fruit ? 1 : -1);
      else if (state.selected.sort === 'Favorite Color')
        sortedList.sort((a, b) => a.preferences.favorite_color > b.preferences.favorite_color ? 1 : -1);
      else if (state.selected.sort === 'Favorite Movie')
        sortedList.sort((a, b) => a.preferences.favorite_movie > b.preferences.favorite_movie ? 1 : -1);

      // Sort ascending or descending
      if (state.selected.order === 'Descending')
        sortedList.reverse();
      
      // Filter according to search input
      return sortedList.filter(person => {
        if (
          person.firstname.toLowerCase().includes(state.search.toLowerCase()) ||
          person.lastname.toLowerCase().includes(state.search.toLowerCase()) ||
          person.contact.email.toLowerCase().includes(state.search.toLowerCase()) ||
          person.contact.country.toLowerCase().includes(state.search.toLowerCase()) ||
          person.preferences.favorite_pet.toLowerCase().includes(state.search.toLowerCase()) ||
          person.preferences.favorite_fruit.toLowerCase().includes(state.search.toLowerCase()) ||
          person.preferences.favorite_color.toLowerCase().includes(state.search.toLowerCase())
        )
            return true;
      })
    },
    PETS: (state, getters) => {
      if (state.list.length > 0) {
        let arrPets = [];
        for (const people of getters.FILTEREDLIST) {
          arrPets.push(people.preferences.favorite_pet);
        }

        let occ = [], prev;
        arrPets.sort();
        for (let i = 0; i < arrPets.length; i++) {
          if (arrPets[i] !== prev) occ.push(1);
          else occ[occ.length - 1]++;
          prev = arrPets[i];
        }
        return {labels: [...new Set(arrPets)], occurrence: occ};
      }
    },
    FRUITS: (state, getters) => {
      if (state.list.length > 0) {
        let arrFruits = [];
        for (const people of getters.FILTEREDLIST) {
          arrFruits.push(people.preferences.favorite_fruit);
        }

        let occ = [], prev;
        arrFruits.sort();
        for (let i = 0; i < arrFruits.length; i++) {
          if (arrFruits[i] !== prev) occ.push(1);
          else occ[occ.length - 1]++;
          prev = arrFruits[i];
        }
        return {labels: [...new Set(arrFruits)], occurrence: occ};
      }
    },
    COLORS: (state, getters) => {
      if (state.list.length > 0) {
        let arrColors = [];
        for (const people of getters.FILTEREDLIST) {
          arrColors.push(people.preferences.favorite_color);
        }

        let occ = [], prev;
        arrColors.sort();
        for (let i = 0; i < arrColors.length; i++) {
          if (arrColors[i] !== prev) occ.push(1);
          else occ[occ.length - 1]++;
          prev = arrColors[i];
        }
        return {labels: [...new Set(arrColors)], occurrence: occ};
      }
    },

    // MODAL
    TOGGLEMODAL: state => state.toggleModal,
    ID: state => state.id,
  },
  actions: {
    // API
    async GET_DATA({ commit }) {
      const res = await fetch("https://run.mocky.io/v3/70e5b0ad-7112-41c5-853e-b382a39e65b7");
      const data = await res.json();
      commit('SET_DATA', data.people);
    }
  },
  mutations: {
    // API
    SET_LOADING(state, payload) {
      state.loading = payload;
    },
    SET_DATA(state, payload) {
      state.list = payload;
    },
    SET_SEARCH(state, payload) {
      state.search = payload;
    },
    SET_SELECT_GENDER(state, payload) {
      state.selected.gender = payload;
    },
    SET_SELECT_SORT(state, payload) {
      state.selected.sort = payload;
    },
    SET_SELECT_ORDER(state, payload) {
      state.selected.order = payload;
    },
    SET_SELECT_FAVORITE(state, payload) {
      state.selected.favorite.label = payload.label;
      state.selected.favorite.data = payload.data;
    },

    // MODAL
    SET_TOGGLEMODAL(state, payload) {
      state.toggleModal = payload;
    },
    SET_ID(state, payload) {
      state.id = payload;
    }
  }
})