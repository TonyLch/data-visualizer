# data-visualizer

## Features
- [x] Fetch data from : https://run.mocky.io/v3/70e5b0ad-7112-41c5-853e-b382a39e65b7
- [x] Table of data paginated
- [x] Some charts about a few data from the list (pie chart & histogram)
- [x] Search Bar for the table of data
- [x] Filter & Sort data
- [x] Show details from each row of table
- [x] Being able to update data (only until refresh, data aren't updated directly on api itself)
- [x] Export updated data in JSON format

### Bonus features
- [x] Filter data via charts interactions **can only filter by one favorite category at a time**
- [ ] Visualize location data through a map
- [ ] Unit tests &/or E2E

## Dependencies
- vue-router
- vuex
- chartjs

More details in package.json

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
